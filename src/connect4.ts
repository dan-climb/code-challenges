const rows = 6;
const columns = 7;

const has4InARow = (line: any[], index) =>
  line.slice(index, index + 4).every((cell) => cell === "Red") ||
  line.slice(index, index + 4).every((cell) => cell === "Yellow");

const isGameOver = (board: any[][], row: number, col: number) => {
  const diff = row - col;
  const column = board.map((row) => row[col]);
  const tltbrdiagonal = board.map((row, i) =>
    i - diff >= 0 ? row[i - diff] : null
  );
  const blttrdiagonal = [...board]
    .reverse()
    .map((row, i) => (i - diff >= 0 ? row[i - diff] : null));

  // horizontal win
  for (let i = 0; i < columns - 4; i++) {
    if (has4InARow(board[row], i)) return true;
  }

  // vertical win
  for (let i = 0; i < rows - 4; i++) {
    if (has4InARow(column, i)) return true;
  }

  // diagonal win
  for (let i = 0; i < rows - 4; i++) {
    if (has4InARow(tltbrdiagonal, i) || has4InARow(blttrdiagonal, i))
      return true;
  }

  return false;
};

export const whoIsWinner = (piecesPositionList: string[]) => {
  const board = Array(rows)
    .fill(null)
    .map(() => Array(columns).fill(null));

  for (const turn of piecesPositionList) {
    const [column, player] = turn.split("_");
    const col = column.charCodeAt(0) - 65;

    for (let row = rows - 1; row >= 0; row--) {
      if (!board[row][col]) {
        board[row][col] = player;
        if (isGameOver(board, row, col)) {
          return player;
        }
        break;
      }
    }
  }

  return "Draw";
};
